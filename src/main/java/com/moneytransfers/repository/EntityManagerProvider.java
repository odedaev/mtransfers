package com.moneytransfers.repository;

import com.moneytransfers.exception.PersistenceException;
import com.moneytransfers.utils.PersistenceUtil;

import javax.persistence.EntityManager;

public class EntityManagerProvider {

    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        if (entityManager == null) {
            throw new PersistenceException("EntityManager is not active");
        }
        return entityManager;
    }

    public void beginTransaction() {
        if (isEntityManagerNotActive()) {
            entityManager = PersistenceUtil.getEntityManager();
        }
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
    }

    public void commitTransaction() {
        if (isEntityManagerNotActive()) {
            throw new PersistenceException("EntityManager is not active");
        }

        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().commit();
            entityManager.close();
        }
    }

    public void rollbackTransaction() {
        if (isEntityManagerNotActive()) {
            throw new PersistenceException("EntityManager is not active");
        }
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().rollback();
            entityManager.close();
        }
    }

    private boolean isEntityManagerNotActive() {
        return entityManager == null || !entityManager.isOpen();
    }
}
