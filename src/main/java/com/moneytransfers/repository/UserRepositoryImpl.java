package com.moneytransfers.repository;

import com.moneytransfers.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    private static final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);

    private final EntityManagerProvider entityManagerProvider;

    public UserRepositoryImpl(EntityManagerProvider entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Nonnull
    public List<User> getAllUsers() {
        return entityManagerProvider.getEntityManager()
                .createNamedQuery(User.SELECT_ALL, User.class).getResultList();
    }

    @Override
    public User getById(long userId) {
        return entityManagerProvider.getEntityManager().find(User.class, userId);
    }

    @Nonnull
    public User save(@Nonnull User user) {
        return entityManagerProvider.getEntityManager().merge(user);
    }
}
