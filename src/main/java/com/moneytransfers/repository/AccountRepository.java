package com.moneytransfers.repository;

import com.moneytransfers.entity.Account;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface AccountRepository {

    /**
     * Return Account by User Id
     *
     * @param userId DB User Id
     * @return List of Accounts or empty list
     */
    @Nonnull
    List<Account> getByUserId(long userId);

    /**
     * Return Account by Id
     *
     * @param accountId DB Account Id
     * @return Account or null
     */
    @Nullable
    Account getById(long accountId);

    /**
     * Save Account into DB
     *
     * @param account Account to save
     * @return saved account
     */
    @Nonnull
    Account save(@Nonnull Account account);
}
