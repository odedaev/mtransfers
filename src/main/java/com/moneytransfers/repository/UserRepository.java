package com.moneytransfers.repository;


import com.moneytransfers.entity.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface UserRepository {

    /**
     * Return all Users.
     *
     * @return List of Users or empty list
     */
    @Nonnull
    List<User> getAllUsers();

    /**
     * Find User by Id
     *
     * @param id User Id
     * @return User or null
     */
    @Nullable
    User getById(long id);

    /**
     * Save the User in DB
     *
     * @param user User entity
     * @return Saved User
     */
    @Nonnull
    User save(@Nonnull User user);
}
