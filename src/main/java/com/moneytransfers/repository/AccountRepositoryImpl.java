package com.moneytransfers.repository;

import com.moneytransfers.entity.Account;

import javax.annotation.Nonnull;
import java.util.List;

public class AccountRepositoryImpl implements AccountRepository {

    private final EntityManagerProvider entityManagerProvider;

    public AccountRepositoryImpl(EntityManagerProvider entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Nonnull
    public List<Account> getByUserId(long userId) {
        return entityManagerProvider.getEntityManager()
                .createNamedQuery(Account.SELECT_BY_USER_ID, Account.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nonnull
    public Account save(@Nonnull Account account) {
        return entityManagerProvider.getEntityManager().merge(account);
    }

    @Nonnull
    public Account getById(long accountId) {
        return entityManagerProvider.getEntityManager()
                .find(Account.class, accountId);
    }
}
