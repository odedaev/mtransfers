package com.moneytransfers.service.dto;

import com.moneytransfers.entity.Account;
import com.moneytransfers.entity.Currency;

import java.math.BigDecimal;
import java.util.Objects;

public class AccountDto {
    private Long id;
    private String name;
    private BigDecimal balance;
    private Currency currency;

    public AccountDto() {
    }

    public AccountDto(Long id, String name, BigDecimal balance, Currency currency) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.currency = currency;
    }

    public AccountDto(Account account) {
        this.id = account.getId();
        this.name = account.getName();
        this.balance = account.getBalance();
        this.currency = account.getCurrency();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDto that = (AccountDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(balance, that.balance) &&
                currency == that.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, balance, currency);
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", currency=" + currency +
                '}';
    }
}
