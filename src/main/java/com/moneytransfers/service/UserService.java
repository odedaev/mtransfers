package com.moneytransfers.service;

import com.moneytransfers.entity.Account;
import com.moneytransfers.entity.User;
import com.moneytransfers.exception.ResourceNotFoundException;
import com.moneytransfers.exception.TransferException;
import com.moneytransfers.repository.*;
import com.moneytransfers.service.dto.AccountDto;
import com.moneytransfers.service.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final EntityManagerProvider entityManagerProvider;

    public UserService() {
        entityManagerProvider = new EntityManagerProvider();
        userRepository = new UserRepositoryImpl(entityManagerProvider);
        accountRepository = new AccountRepositoryImpl(entityManagerProvider);
    }

    public UserService(UserRepository userRepository, AccountRepository accountRepository, EntityManagerProvider entityManagerProvider) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.entityManagerProvider = entityManagerProvider;
    }

    /**
     * Return User by Id
     *
     * @param userId DB User Id
     * @return User or null
     */
    @Nullable
    public UserDto getUserById(long userId) {
        entityManagerProvider.beginTransaction();
        User user = userRepository.getById(userId);
        entityManagerProvider.commitTransaction();
        return user != null ? new UserDto(user) : null;
    }

    /**
     * Return list of all Users in DB
     *
     * @return list of Users or empty list
     */
    @Nonnull
    public List<UserDto> getAllUsers() {
        entityManagerProvider.beginTransaction();
        List<User> users = userRepository.getAllUsers();
        entityManagerProvider.commitTransaction();
        return users.stream().map(UserDto::new).collect(Collectors.toList());
    }

    /**
     * Insert the new User in DB
     *
     * @param userDto User to insert
     * @return saved User
     */
    public UserDto addNewUser(UserDto userDto) {
        try {
            entityManagerProvider.beginTransaction();

            User user = new User();
            user = userRepository.save(mergeWithDto(user, userDto));

            entityManagerProvider.commitTransaction();

            return new UserDto(user);
        } catch (RuntimeException e) {
            entityManagerProvider.rollbackTransaction();
            logger.error("Can't insert the new user");
            throw e;
        }
    }

    /**
     * Update the User in DB
     *
     * @param userDto User to update
     * @return updated User
     */
    public UserDto updateUser(UserDto userDto) {
        try {
            entityManagerProvider.beginTransaction();

            User user = userRepository.getById(userDto.getId());
            // user not found
            if (user == null) {
                entityManagerProvider.commitTransaction();
                return null;
            }
            user = mergeWithDto(user, userDto);
            user = userRepository.save(user);

            entityManagerProvider.commitTransaction();
            return new UserDto(user);
        } catch (RuntimeException e) {
            entityManagerProvider.rollbackTransaction();
            logger.error("Can't update the User. Id = " + userDto.getId());
            throw e;
        }
    }

    /**
     * Return Accounts by User Id
     *
     * @param userId DB User Id
     * @return list of Accounts or empty list
     */
    public List<AccountDto> getAccountsByUserId(long userId) {
        entityManagerProvider.beginTransaction();
        List<Account> accounts = accountRepository.getByUserId(userId);
        entityManagerProvider.commitTransaction();
        return accounts.stream().map(AccountDto::new).collect(Collectors.toList());
    }

    /**
     * Insert new Account for User
     *
     * @param userId     DB User Id
     * @param accountDto Account to save
     * @return Saved Account
     */
    public AccountDto addNewAccountForUser(long userId, AccountDto accountDto) {
        try {
            entityManagerProvider.beginTransaction();

            User user = userRepository.getById(userId);
            if (user == null) {
                throw new ResourceNotFoundException("User not found. Id = " + userId);
            }

            Account account = new Account(user);
            account = accountRepository.save(mergeWithDto(account, accountDto));

            entityManagerProvider.commitTransaction();

            return new AccountDto(account);
        } catch (RuntimeException e) {
            entityManagerProvider.rollbackTransaction();
            logger.error("Can't insert the new account", e);
            throw e;
        }
    }

    /**
     * Transfer money from one Account to another
     *
     * @param fromAccountId Source Account
     * @param toAccountId   Destination Account
     * @param amount        amount to transfer
     */
    public void transferMoney(long fromAccountId, long toAccountId, BigDecimal amount) {
        try {
            entityManagerProvider.beginTransaction();
            Account fromAccount = accountRepository.getById(fromAccountId);
            if (fromAccount == null) {
                throw new TransferException("Account doesn't exist. Id = " + fromAccountId);
            }

            Account toAccount = accountRepository.getById(toAccountId);
            if (toAccount == null) {
                throw new TransferException("Account doesn't exist. Id = " + toAccountId);
            }

            if (fromAccount.getCurrency() != toAccount.getCurrency()) {
                throw new TransferException("Currencies are different");
            }

            amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            if (fromAccount.getBalance().compareTo(amount) < 0) {
                throw new TransferException("Not enough money to transfer");
            }

            fromAccount.setBalance(fromAccount.getBalance().subtract(amount));
            toAccount.setBalance(toAccount.getBalance().add(amount));

            accountRepository.save(fromAccount);
            accountRepository.save(toAccount);

            entityManagerProvider.commitTransaction();
        } catch (RuntimeException e) {
            entityManagerProvider.rollbackTransaction();
            logger.error("Can't transfer money", e);
            throw e;
        }
    }

    private User mergeWithDto(User user, UserDto userDto) {
        user.setLastName(userDto.getLastName());
        user.setFirstName(userDto.getFirstName());
        user.setEmail(userDto.getEmail());
        return user;
    }

    private Account mergeWithDto(Account account, AccountDto accountDto) {
        account.setCurrency(accountDto.getCurrency());
        account.setName(accountDto.getName());
        account.setBalance(accountDto.getBalance().setScale(2, BigDecimal.ROUND_HALF_EVEN));
        return account;
    }
}
