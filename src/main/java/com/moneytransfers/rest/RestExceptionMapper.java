package com.moneytransfers.rest;

import com.moneytransfers.exception.ResourceNotFoundException;
import com.moneytransfers.rest.vo.ExceptionResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/**
 * Maps all Exceptions in JSON response
 */
@Provider
public class RestExceptionMapper implements ExceptionMapper<Exception> {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionMapper.class);

    @Override
    public Response toResponse(Exception exception) {

        logger.error(exception.getMessage(), exception);

        ExceptionResponse exceptionResponse = new ExceptionResponse(exception.getMessage(),
                exception.getClass().getName());

        if (isCausedBy(exception, ResourceNotFoundException.class)) {
            return Response.status(Response.Status.NO_CONTENT).entity(exceptionResponse)
                    .type(MediaType.APPLICATION_JSON).build();
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(exceptionResponse)
                .type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    private boolean isCausedBy(Throwable throwable, Class clazz) {
        return ExceptionUtils.indexOfType(throwable, clazz) >= 0;
    }
}
