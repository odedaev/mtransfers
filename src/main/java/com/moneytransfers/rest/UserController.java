package com.moneytransfers.rest;

import com.moneytransfers.exception.ResourceNotFoundException;
import com.moneytransfers.service.UserService;
import com.moneytransfers.service.dto.AccountDto;
import com.moneytransfers.service.dto.UserDto;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    private UserService userService;


    @GET
    @Path("/{userId}")
    public UserDto getUserById(@PathParam("userId") long userId) {
        return getUser(userId);
    }

    /**
     * Return all users
     *
     * @return List of Users or empty List
     */
    @GET
    public List<UserDto> getAllUsers() {
        //TODO: add pagination
        return userService.getAllUsers();
    }

    @POST
    public UserDto addUser(UserDto userDto) {
        if (userDto == null) {
            throw new BadRequestException("Body is empty");
        }
        return userService.addNewUser(userDto);
    }

    @PUT
    @Path("/{userId}")
    public Response updateUser(@PathParam("userId") long userId, UserDto userDto) {
        userDto.setId(userId);
        userDto = userService.updateUser(userDto);
        return userDto != null ? Response.status(Response.Status.OK).build()
                : Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/{userId}/accounts")
    public List<AccountDto> getUserAccounts(@PathParam("userId") long userId) {
        return userService.getAccountsByUserId(userId);
    }

    @POST
    @Path("/{userId}/accounts")
    public AccountDto addAccount(@PathParam("userId") long userId, AccountDto accountDto) {
        if (accountDto == null) {
            throw new BadRequestException("Body is empty");
        }
        return userService.addNewAccountForUser(userId, accountDto);
    }

    private UserDto getUser(long userId) {
        UserDto user = userService.getUserById(userId);
        if (user == null) {
            throw new ResourceNotFoundException("User not found. User Id = " + userId);
        }
        return user;
    }
}
