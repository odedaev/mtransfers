package com.moneytransfers.rest;

import com.moneytransfers.rest.vo.TransferBody;
import com.moneytransfers.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {

    @Inject
    private UserService userService;

    /**
     * Transfer money from one account to another
     *
     * @param accountId    Source Account Id
     * @param transferBody Destination Account Id
     * @return code 200 if success
     */
    @PUT
    @Path("/{accountId}/transfer")
    public Response transferMoney(@PathParam("accountId") long accountId, TransferBody transferBody) {
        if (transferBody == null) {
            throw new BadRequestException("Body is empty");
        }

        if (transferBody.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new BadRequestException("Amount is negative");
        }

        userService.transferMoney(accountId, transferBody.getAccountId(), transferBody.getAmount());
        return Response.status(Response.Status.OK).build();
    }
}
