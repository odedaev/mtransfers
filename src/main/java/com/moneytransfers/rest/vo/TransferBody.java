package com.moneytransfers.rest.vo;

import java.math.BigDecimal;
import java.util.Objects;

public class TransferBody {
    private long accountId;
    private BigDecimal amount;

    public TransferBody() {
    }

    public TransferBody(long accountId, BigDecimal amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransferBody that = (TransferBody) o;
        return accountId == that.accountId &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, amount);
    }

    @Override
    public String toString() {
        return "TransferBody{" +
                "accountId=" + accountId +
                ", amount=" + amount +
                '}';
    }
}
