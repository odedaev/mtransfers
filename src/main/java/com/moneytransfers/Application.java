package com.moneytransfers;

import com.moneytransfers.rest.AccountController;
import com.moneytransfers.rest.RestExceptionMapper;
import com.moneytransfers.rest.UserController;
import com.moneytransfers.service.UserService;
import com.moneytransfers.utils.PersistenceUtil;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);


    public static void main(String[] args) throws Exception {
        logger.info("Starting application");

        ResourceConfig config = new ResourceConfig();
        config.register(UserController.class);
        config.register(AccountController.class);
        config.register(RestExceptionMapper.class);

        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(UserService.class).to(UserService.class).in(Singleton.class);
            }
        });

        ServletHolder jerseyServlet
                = new ServletHolder(new ServletContainer(config));

        Server server = new Server(8080);
        ServletContextHandler context
                = new ServletContextHandler(server, "/");
        context.addServlet(jerseyServlet, "/*");


        context.addLifeCycleListener(new AbstractLifeCycle.AbstractLifeCycleListener() {
            @Override
            public void lifeCycleStarting(LifeCycle event) {
                PersistenceUtil.buildEntityManagerFactory();
            }

            @Override
            public void lifeCycleStopping(LifeCycle event) {
                PersistenceUtil.killEntityManagerFactory();
            }
        });

        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }
}
