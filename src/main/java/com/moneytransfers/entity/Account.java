package com.moneytransfers.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@NamedQueries({
        @NamedQuery(name = Account.SELECT_BY_USER_ID, query = "select a from Account a where a.user.id = :userId")
})
public class Account {
    public final static String SELECT_BY_USER_ID = "User.getByUserId";

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @Fetch(FetchMode.JOIN)
    private User user;

    @Column(nullable = false)
    private String name;

    @Column(scale = 2, nullable = false)
    private BigDecimal balance = new BigDecimal(0);

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Currency currency;

    public Account() {
    }

    public Account(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
