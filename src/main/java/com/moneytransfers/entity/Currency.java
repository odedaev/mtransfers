package com.moneytransfers.entity;

public enum Currency {
    RUB,
    USD,
    EUR;
}
