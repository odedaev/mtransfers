INSERT INTO User (id, firstName, lastName, email) VALUES (1, 'John', 'Smith', 'js@gmail.com');
INSERT INTO User (id, firstName, lastName, email) VALUES (2, 'Some', 'Other', 'so@gmail.com');

INSERT INTO Account (id, user_id, name, balance, currency) VALUES (3, 1, 'For College', 100, 'USD');
INSERT INTO Account (id, user_id, name, balance, currency) VALUES (4, 1, 'Food', 1000, 'RUB');

INSERT INTO Account (id, user_id, name, balance, currency) VALUES (5, 2, 'For Live', 1000, 'USD');
INSERT INTO Account (id, user_id, name, balance, currency) VALUES (6, 2, 'Car', 1500, 'RUB');