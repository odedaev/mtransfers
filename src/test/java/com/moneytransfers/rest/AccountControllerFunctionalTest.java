package com.moneytransfers.rest;

import com.moneytransfers.AbstractFunctionalTest;
import com.moneytransfers.RestUtil;
import com.moneytransfers.rest.vo.TransferBody;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AccountControllerFunctionalTest extends AbstractFunctionalTest {

    private HttpClient client;


    @Before
    public void before() {
        client = HttpClients.createDefault();
    }

    @After
    public void after()  {
        HttpClientUtils.closeQuietly(client);
    }

    @Test
    public void testTransferMoney() throws IOException {
        TransferBody transferBody = new TransferBody(5, new BigDecimal(50));

        HttpPut httpPut = RestUtil.createPut(HOST + "/accounts/3/transfer", objectMapper.writeValueAsString(transferBody));
        HttpResponse httpResponse = client.execute(httpPut);

        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
    }
}
