package com.moneytransfers.rest;

import com.moneytransfers.AbstractFunctionalTest;
import com.moneytransfers.RestUtil;
import com.moneytransfers.service.dto.UserDto;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserControllerFunctionalTest extends AbstractFunctionalTest {

    private HttpClient client;


    @Before
    public void before() {
        client = HttpClients.createDefault();
    }

    @After
    public void after()  {
        HttpClientUtils.closeQuietly(client);
    }

    /**
     * Test GetUserById REST point
     */
    @Test
    public void testGetUserById() throws IOException {
        HttpGet httpGet = RestUtil.createGet(HOST + "/users/1");
        HttpResponse httpResponse = client.execute(httpGet);

        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);

        UserDto userDto = objectMapper.readValue(EntityUtils.toString(httpResponse.getEntity()), UserDto.class);
        assertThat(userDto.getId()).isEqualTo(1L);
    }

    /**
     * Test POST new User request
     */
    @Test
    public void testPostNewUser() throws IOException {
        UserDto bodyUserDto = new UserDto(null, "Test", "User", "test@email.com");

        HttpPost httpPost = RestUtil.createPost(HOST + "/users", objectMapper.writeValueAsString(bodyUserDto));
        HttpResponse httpResponse = client.execute(httpPost);

        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);

        UserDto userDto = objectMapper.readValue(EntityUtils.toString(httpResponse.getEntity()), UserDto.class);
        assertThat(userDto.getId()).isNotNull();
        assertThat(userDto.getFirstName()).isEqualTo(userDto.getFirstName());
        assertThat(userDto.getLastName()).isEqualTo(userDto.getLastName());
        assertThat(userDto.getEmail()).isEqualTo(userDto.getEmail());
    }
}
