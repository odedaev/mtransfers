package com.moneytransfers.rest;

import com.moneytransfers.exception.ResourceNotFoundException;
import com.moneytransfers.service.UserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.BadRequestException;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @Mock
    private UserService userService;
    @InjectMocks
    private UserController userController;

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    /**
     * If body is null the BadRequestException should be thrown
     */
    @Test
    public void testAddUserWithEmptyBody() {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage("Body is empty");

        userController.addUser(null);
    }

    /**
     * If user not found the Controller should throw Exception
     */
    @Test
    public void testGetUserByIdUserNotFound() {
        thrown.expect(ResourceNotFoundException.class);
        thrown.expectMessage("User not found. User Id = 1");

        userController.getUserById(1L);
    }
}