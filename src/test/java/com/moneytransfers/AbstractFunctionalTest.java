package com.moneytransfers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneytransfers.rest.AccountController;
import com.moneytransfers.rest.RestExceptionMapper;
import com.moneytransfers.rest.UserController;
import com.moneytransfers.service.UserService;
import com.moneytransfers.utils.PersistenceUtil;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.BeforeClass;

import javax.inject.Singleton;

public abstract class AbstractFunctionalTest {
    protected static final int SERVER_PORT = 8081;
    protected static final String HOST = "http://localhost:" + SERVER_PORT;

    protected static Server server;
    protected static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void beforeClass() throws Exception {
        if (server == null) {
            configureServer();
        }
    }

    private static void configureServer() throws Exception {
        ResourceConfig config = new ResourceConfig();
        config.register(UserController.class);
        config.register(AccountController.class);
        config.register(RestExceptionMapper.class);

        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(UserService.class).to(UserService.class).in(Singleton.class);
            }
        });

        ServletHolder jerseyServlet
                = new ServletHolder(new ServletContainer(config));

        server = new Server(SERVER_PORT);
        ServletContextHandler context
                = new ServletContextHandler(server, "/");
        context.addServlet(jerseyServlet, "/*");

        context.addLifeCycleListener(new AbstractLifeCycle.AbstractLifeCycleListener() {
            @Override
            public void lifeCycleStarting(LifeCycle event) {
                PersistenceUtil.buildEntityManagerFactory();
            }

            @Override
            public void lifeCycleStopping(LifeCycle event) {
                PersistenceUtil.killEntityManagerFactory();
            }
        });

        server.start();
    }
}
