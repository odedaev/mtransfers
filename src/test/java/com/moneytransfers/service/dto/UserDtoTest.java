package com.moneytransfers.service.dto;

import com.moneytransfers.entity.User;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserDtoTest {

    /**
     * Test correct DTO mapping
     */
    @Test
    public void testMapping() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("Some Name");
        user.setLastName("Some Last Name");
        user.setEmail("Some Email");

        UserDto userDto = new UserDto(user);

        assertThat(userDto.getId()).isEqualTo(user.getId());
        assertThat(userDto.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(userDto.getLastName()).isEqualTo(user.getLastName());
        assertThat(userDto.getEmail()).isEqualTo(user.getEmail());
    }
}