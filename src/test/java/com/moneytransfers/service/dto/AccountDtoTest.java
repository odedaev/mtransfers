package com.moneytransfers.service.dto;

import com.moneytransfers.entity.Account;
import com.moneytransfers.entity.Currency;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class AccountDtoTest {

    /**
     * Test correct DTO mapping
     */
    @Test
    public void testMapping() {
        Account account = new Account();
        account.setId(1L);
        account.setName("Some name");
        account.setBalance(new BigDecimal(100));
        account.setCurrency(Currency.RUB);

        AccountDto accountDto = new AccountDto(account);

        assertThat(accountDto.getId()).isEqualTo(account.getId());
        assertThat(accountDto.getName()).isEqualTo(account.getName());
        assertThat(accountDto.getBalance()).isEqualTo(account.getBalance());
        Assertions.assertThat(accountDto.getCurrency()).isEqualTo(account.getCurrency());
    }
}