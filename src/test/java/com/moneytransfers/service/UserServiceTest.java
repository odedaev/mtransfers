package com.moneytransfers.service;

import com.moneytransfers.entity.User;
import com.moneytransfers.repository.AccountRepository;
import com.moneytransfers.repository.EntityManagerProvider;
import com.moneytransfers.repository.UserRepository;
import com.moneytransfers.service.dto.UserDto;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private static final long USER_ID = 1L;


    @Mock
    private UserRepository userRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private EntityManagerProvider entityManagerProvider;

    @InjectMocks
    private UserService userService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private User user;

    @Before
    public void before() {
        user = new User();
        user.setId(USER_ID);
        user.setFirstName("Some F Name");
        user.setLastName("Some L Name");
        user.setEmail("email@email.com");
    }

    /**
     * Should return not null User
     */
    @Test
    public void testGetUserById() {
        when(userRepository.getById(eq(USER_ID))).thenReturn(user);
        UserDto userDto = userService.getUserById(1L);
        assertThat(userDto).isNotNull();
    }

    /**
     * If User not found then null
     */
    @Test
    public void testGetUserByIdRerunNull() {
        UserDto userDto = userService.getUserById(1L);
        assertThat(userDto).isNull();
    }

    /**
     * If DB response is empty list then service return empty list
     */
    @Test
    public void testGetAllUsers() {
        when(userRepository.getAllUsers()).thenReturn(Collections.emptyList());
        List<UserDto> userDtos = userService.getAllUsers();

        assertThat(userDtos).isNotNull();
        assertThat(userDtos).hasSize(0);
    }
}