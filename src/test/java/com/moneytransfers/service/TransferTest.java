package com.moneytransfers.service;

import com.moneytransfers.entity.Account;
import com.moneytransfers.entity.Currency;
import com.moneytransfers.exception.TransferException;
import com.moneytransfers.repository.AccountRepository;
import com.moneytransfers.repository.EntityManagerProvider;
import com.moneytransfers.repository.UserRepository;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransferTest {

    private static final long ACCOUNT_ID1 = 1L;
    private static final long ACCOUNT_ID2 = 2L;

    @Mock
    private UserRepository userRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private EntityManagerProvider entityManagerProvider;

    @InjectMocks
    private UserService userService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Account account1;
    private Account account2;

    @Before
    public void before() {
        account1 = new Account();
        account1.setId(ACCOUNT_ID1);
        account1.setBalance(new BigDecimal(100));
        account1.setCurrency(Currency.RUB);

        account2 = new Account();
        account2.setId(ACCOUNT_ID2);
        account2.setBalance(new BigDecimal(50));
        account2.setCurrency(Currency.RUB);
    }

    /**
     * Test positive transfer
     */
    @Test
    public void testTransfer() {
        when(accountRepository.getById(ACCOUNT_ID1)).thenReturn(account1);
        when(accountRepository.getById(ACCOUNT_ID2)).thenReturn(account2);

        userService.transferMoney(ACCOUNT_ID1, ACCOUNT_ID2, new BigDecimal(100));

        ArgumentCaptor<Account> parameterCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository, times(2)).save(parameterCaptor.capture());

        parameterCaptor.getAllValues().forEach(acc -> {
            if (acc.getId().equals(ACCOUNT_ID1)) {
                //test balance of the first account
                assertThat(acc.getBalance()).isEqualTo(new BigDecimal("0.00"));
            } else if (acc.getId().equals(ACCOUNT_ID2)) {
                //test balance of the first account
                assertThat(acc.getBalance()).isEqualTo(new BigDecimal("150.00"));
            }
        });
    }

    /**
     * Negative. Account not found. Throws Exception
     */
    @Test
    public void testAccountNotFound() {
        thrown.expect(TransferException.class);
        thrown.expectMessage(Matchers.startsWith("Account doesn't exist. Id = "));

        userService.transferMoney(ACCOUNT_ID1, ACCOUNT_ID2, new BigDecimal(100));
    }

    /**
     * Negative. If currencies are different then throws Exception
     */
    @Test
    public void testCurrenciesAreDiffirent() {
        when(accountRepository.getById(ACCOUNT_ID1)).thenReturn(account1);
        when(accountRepository.getById(ACCOUNT_ID2)).thenReturn(account2);

        account2.setCurrency(Currency.USD);

        thrown.expect(TransferException.class);
        thrown.expectMessage("Currencies are different");

        userService.transferMoney(ACCOUNT_ID1, ACCOUNT_ID2, new BigDecimal(100));
    }
}