package com.moneytransfers;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;

public class RestUtil {

    public static HttpGet createGet(String url) {
        HttpGet request = new HttpGet(url);
        request.setHeader("Content-type", "application/json");
        return request;
    }

    public static HttpPost createPost(String url, String body) throws UnsupportedEncodingException {
        HttpEntity httpEntity = new StringEntity(body);
        HttpPost request = new HttpPost(url);
        request.setHeader("Content-type", "application/json");
        request.setEntity(httpEntity);
        return request;
    }

    public static HttpPut createPut(String url, String body) throws UnsupportedEncodingException {
        HttpEntity httpEntity = new StringEntity(body);
        HttpPut request = new HttpPut(url);
        request.setHeader("Content-type", "application/json");
        request.setEntity(httpEntity);
        return request;
    }
}
