# Money transfer Service

REST API for money transfer.


## How to run

```text
gradlew run
```

Server starts on [localhost:8080](localhost:8080)  
DB contains Test Data:  
- 2 Users
- 2 accounts for each User

 
## REST Points:
**GET** /users/ - return list of users  
**GET** /users/{userId}/ - return specific User by Id  
**POST** /users/ - create the new User  
**PUT** /users/{userId}/ - update User by Id
  
**POST/PUT User Body**
```json
{
    "firstName": "",
    "lastName": "",
    "email": ""
}
```

**GET** /users/{userId}/accounts - return list of Accounts for User by User Id  
**POST** /users/{userId}/accounts - add new Account for User

**POST Account Body**
```json
{
    "name": "",
    "balance": 0,
    "currency": ""
}
```
Body ***currency*** - support only [RUB, USD, EUR]

**PUT** /accounts/{accountId}/transfer - transfer money from an Account to another

**PUT Transfer Body**
```json
{
    "accountId": 0,
    "amount": 0
}
```
Path variable ***{accountId}*** - source Account  
Body ***accountId*** - destination Account 


## TODO:
- add Swagger
- add DBUnit for Functional tests
- increase Test Coverage
- provide additional Rest points to delete / update Users and Accounts